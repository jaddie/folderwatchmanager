﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FolderWatchManager
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			InitializeComponent();
		}

		private void BtnSettings_Click(object sender, RoutedEventArgs e)
		{

		}

		private void ListViewSource_DragEnter(object sender, DragEventArgs e)
		{
			if (e.Data.GetDataPresent(DataFormats.FileDrop))
			{
				e.Effects = DragDropEffects.Copy;
			}
			else
			{
				e.Effects = DragDropEffects.None;
			}
		}

		private void ListViewSource_Drop(object sender, DragEventArgs e)
		{
			//foreach (var visualYSnappingGuideline in e.Data.GetFormats())
			//{
			//	MessageBox.Show(visualYSnappingGuideline);
			//}
			foreach (var val in (IEnumerable<string>) e.Data.GetData("FileDrop"))
			{
				MessageBox.Show(val);
			}

			foreach (var val in (IEnumerable<string>)e.Data.GetData("FileNameW"))
			{
				MessageBox.Show(val);
			}

			foreach (var val in (IEnumerable<string>)e.Data.GetData("FileName"))
			{
				MessageBox.Show(val);
			}
		}

		private void ListViewDestination_Drop(object sender, DragEventArgs e)
		{

		}
	}
}
